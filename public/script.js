document.querySelector('#solve').addEventListener(
    'click',
    () => {
        const green_letters = document.querySelector('#fixed').value.padEnd(5, ' ').toLowerCase();
        const yellow_letters = [1, 2, 3, 4, 5].map(i => document.querySelector('#yellow' + i).value.padEnd(5, ' ').toLowerCase());
        const excluded_letters = Array.from(document.querySelector('#excluded').value.toLowerCase())
        console.log(green_letters, Array.from(green_letters))
        console.log("excluded", excluded_letters)

        let filtered_words = all_words.filter(
            word => {
                return Array.from(green_letters)
                    .filter((letter, index) => letter !== ' ' && letter !== word[index])
                    .length === 0
            }
        )
        console.log("length after green: ", filtered_words.length)
        filtered_words = filtered_words.filter(
            word => excluded_letters.filter(
                letter => word.includes(letter)
            ).length === 0
        )

        for (const yellow_word of yellow_letters) {
            filtered_words = filtered_words.filter(
                word => {
                    return Array.from(yellow_word)
                        .filter(i => i !== ' ')
                        .filter(i => !word.includes(i))
                        .length === 0
                }
            )
            console.log("length after contains filter", filtered_words.length)

            filtered_words = filtered_words.filter(
                word => Array.from(yellow_word)
                    .filter((letter, index) => letter !== ' ' && letter === word[index])
                    .length === 0
            )

            console.log("Length after position filter", filtered_words.length)
        }

        const results = document.querySelector('.results')
        while (results.firstChild) {
            results.removeChild(results.firstChild)
        }

        filtered_words.map(
            word => {
                const div = document.createElement('div');
                div.innerText = word;
                results.appendChild(div);
            }
        )
    }
)